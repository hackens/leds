let red_signal, red_signal_set = React.S.create 0
let green_signal, green_signal_set = React.S.create 0
let blue_signal, blue_signal_set = React.S.create 0
let number_signal, number_signal_set = React.S.create 13

let main_listener =
  React.S.l4 (fun r g b n ->
      for i = 0 to n-1 do
        Dev.send_to_leds r g b i
      done;
    ) red_signal green_signal blue_signal number_signal
  |> Lwt_react.S.keep

(*
let main_loop =
  let dev = find_dev () in
  Format.eprintf "Opening device %s.@." dev;
  let com = open_com dev in
  let k = 0 in
  let i = ref 0 in
  while%lwt true do
    let red = [0x80 + k; !i mod 127; (1* !i) mod 127;  (1* !i) mod 127] in
    let b = Bytes.create 4 in
    List.iteri (fun i c ->
        Bytes.set b i (char_of_int c)) red;
    Unix.write com b 0 4;
    i := !i + 1;
    Lwt_unix.sleep 0.01
  done


let _ = Lwt_main.run main_loop*)
