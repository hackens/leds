exception Break

let launch_no_fork port =
  let num = Config.led_count in
  let open Unix in
  let fd = socket PF_INET SOCK_STREAM 0 in
  let () = bind fd (ADDR_INET(inet_addr_any, port)) in
  let () = Unix.listen fd 1 in
  Format.printf "FFT server launched@.";
  while true do
    let fd, client_addr = accept fd in
    begin
      match client_addr with
      | ADDR_INET(addr, port) ->
        Format.printf "Connection from %s:%d@." (string_of_inet_addr addr) (port)
      | _ -> Format.printf "Connection with a Unix socket (unexpected).@."
    end;
    let in_ch = in_channel_of_descr fd in
    try
      while true do
        let c = try
            input_char in_ch
          with
            Unix.Unix_error(_) | End_of_file | Sys_error(_) -> raise Break
        in
        let c = int_of_char c in
        let c = c mod 127 in
        for i = 0 to c do
          Format.printf "-";
        done;
        Format.printf "%d@." c;
        for i = 0 to num - 1 do
          Dev.send_to_leds c c c 0
        done;
      done;
    with
    | Break ->
      Format.printf "Connection closed.@.";
  done; 0

let launch port =
  match Unix.fork () with
  | 0 ->
    launch_no_fork port
  | k -> k
