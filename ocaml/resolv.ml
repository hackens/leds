let resolv name =
  let open Unix in
  let addrs = getaddrinfo name "" [] in
  let inet_addr = ref None in
  List.iter (fun a ->
      match a.ai_addr with
      | ADDR_INET(inet, _) -> inet_addr := Some inet;
      | _ -> ()) addrs;
  !inet_addr


