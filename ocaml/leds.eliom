[%%shared
    open Eliom_lib
    open Eliom_content
    open Html5.D
    module Int_list = Deriving_Json.Json_list(Deriving_Json.Json_int)
]

module Leds_app =
  Eliom_registration.App (
    struct
      let application_name = "leds"
    end)

let main_service =
  Eliom_service.App.service ~path:[] ~get_params:Eliom_parameter.unit ()

let () = Sys.set_signal Sys.sigchld Sys.Signal_ignore 

let set_color = Eliom_react.Up.create (Eliom_parameter.ocaml "color_and_value" Int_list.t)
let trigger_anim = Eliom_react.Up.create (Eliom_parameter.ocaml "anim" Deriving_Json.Json_string.t)
let set_number = Eliom_react.Up.create (Eliom_parameter.ocaml "number" Deriving_Json.Json_int.t)

let last_anim_played =
  let last_pid = ref (-1) in
  Eliom_react.Up.to_react trigger_anim
  |> React.E.map (fun name ->
      begin
        try
          if !last_pid > 0 then
            begin
              Unix.kill !last_pid 9;
              last_pid := -1;
            end
        with
        | Unix.Unix_error(_) ->
          last_pid := -1;
      end;
      try
        let n = List.length Animation.anims in
        for i = 0 to n - 1 do
          let _, n = List.nth Animation.anims i in
          if n = name then
            begin
              Ocsigen_messages.errlog (Sys.getcwd () ^ "/trigger_anim.native");
              let pid = Unix.create_process (Sys.getcwd () ^ "/trigger_animation.native") [|string_of_int i; string_of_int i|] Unix.stdin Unix.stdout Unix.stderr in
              last_pid := pid;
            end
        done;
        name

        
      with
      | Not_found ->
        Ocsigen_messages.errlog "unknown anim"; "none"
    )
  |> React.S.hold "none"

let () =
  Leds_app.register
    ~service:main_service
    (fun () () ->
       let colors = [0;1;2] in
       let last_anim = Eliom_react.S.Down.of_react last_anim_played in
       let last_anim = [%client
         React.S.map (fun n ->
             span [pcdata ("Playing or last played: " ^ n)]) ~%last_anim
         |> Html5.R.node
       ] |> Html5.C.node in
       let slider_num = 
           Html5.D.(Form.input
                      ~a:[
                        a_input_min 1.;
                        a_input_max 25.
                      ]
                      ~input_type:`Range
                      Form.int)
       in
       let sliders = List.map (fun i ->
           Html5.D.(Form.input
                      ~a:[
                        a_input_min 0.;
                        a_input_max 127.
                      ]
                      ~input_type:`Range
                      Form.int), i) colors in
       let anims = Animation.anims in
       let links = List.map (fun (_, name) ->
           let a = Html5.D.(a [pcdata name] ~service:main_service ()) in
           let _ = [%client
             Lwt_js_events.async (fun () ->
                 let a = Eliom_content.Html5.To_dom.of_a ~%a in
                 Lwt_js_events.clicks a (fun e _ ->
                     Dom.preventDefault e;
                     ~%trigger_anim ~%name |> ignore;
                     Lwt.return_unit)
               );
              ] in
           li [a]) anims in
       let _ = [%client
         Lwt_js_events.async (fun () ->
             Lwt_list.iter_p (fun (s, i) ->
             let slider = Eliom_content.Html5.To_dom.of_input s in
             Lwt_js_events.inputs slider (fun _ _ ->
                 let value = Js.to_string slider##.value |> int_of_string in
                 ~%set_color [i; value] |> ignore;
                 Lwt.return_unit);) ~%sliders
           );
         Lwt_js_events.async (fun () ->
             let slider = Eliom_content.Html5.To_dom.of_input ~%slider_num in
             Lwt_js_events.inputs slider (fun _ _ ->
                 let value = Js.to_string slider##.value |> int_of_string in
                 ~%set_number value |> ignore;
                 Lwt.return_unit);
           );
          ] in
       let sliders = List.map (fun (a, i) ->
           [li [pcdata (Format.sprintf "Color: %d@." i)]; li [a]]) sliders in
       let links = links @ (List.concat sliders) @ [li [pcdata "Leds count"]; li [slider_num]] in
       Lwt.return
         (Eliom_tools.F.html
            ~title:"leds"
            ~css:[["css";"leds.css"]]
            ~other_head:Html5.F.[meta
                                   ~a:[a_name "viewport";
                                       a_content "user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi"]
                                   ();]
            Html5.F.(body 
                       [h1 [pcdata "leds"]; last_anim; 
                        div [ul links];]
                    )))

let _ =
  Eliom_react.Up.to_react set_color
  |> React.E.map (function
      | [0; color] -> Main.red_signal_set color
      | [1; color] -> Main.green_signal_set color
      | [2; color] -> Main.blue_signal_set color
      | _ -> Ocsigen_messages.errlog "unknown color"
    )
  |> Lwt_react.E.keep

let _ =
  Eliom_react.Up.to_react set_number
  |> React.E.map Main.number_signal_set
  |> Lwt_react.E.keep
