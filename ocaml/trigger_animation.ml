let () =
  let n = Array.length Sys.argv in
  if n = 1 then
    Format.printf "Please provide anim number"
  else
    begin
      let f = List.nth Animation.anims (int_of_string (Sys.argv.(1))) |> fst in
      let send_to = (fun led_n (r, g, b) ->
            match led_n with
            | Some i ->
              Dev.send_to_leds r g b i
            | None ->
              for i = 0 to Config.led_count - 1 do
                Dev.send_to_leds r g b i
              done
          )
      in
      f send_to Config.led_count
      ; Dev.close_leds ()
    end
