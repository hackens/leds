module FFT = Fftw3.S
      
let dim = 1024
let input  = FFT.Array1.create FFT.complex Bigarray.c_layout dim
let output = FFT.Array1.create FFT.complex Bigarray.c_layout dim
let dft    = FFT.Array1.dft FFT.Forward input output

let do_fft i =
  let k = Bigarray.Array1.of_array  FFT.complex Bigarray.c_layout i in
  Bigarray.Array1.blit k input;
  FFT.exec dft;
  let k = Array.init (dim/2) (fun i -> Bigarray.Array1.get output (i+1)) in
  k

