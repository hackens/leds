open Pulseaudio
open Lwt

let attr =
    {
      max_length = -1;
      target_length = -1;
      prebuffering = -1;
      min_request = -1;
      fragment_size = 1024;
    }

let sound_analysis svr port =
  let () = Sys.set_signal Sys.sigpipe Sys.Signal_ignore in
  let open Unix in
  let out_ch_fd = Array.make (Array.length svr) None in
  let rec send_fft v =
    for i = 0 to Array.length svr - 1 do
      match out_ch_fd.(i) with
      | Some s ->
        begin
          match Lwt.state s with
          | Return s -> 
            let out_ch, fd = s in
            begin
              try
                let c = char_of_int (v mod 256) in
                output_char out_ch c;
                flush out_ch
              with
              | Sys_error(_) ->
                Unix.close fd;
                Format.printf "Disconnected.@.";
                out_ch_fd.(i) <- None;
            end
          | Sleep -> ()
          | Fail(_) ->
            assert false
        end
      | None ->
        let fd = socket PF_INET SOCK_STREAM 0 in
          match Resolv.resolv svr.(i) with
          | Some inet ->
            let t =
              try%lwt
                let%lwt () = Lwt_unix.connect (Lwt_unix.of_unix_file_descr fd) (ADDR_INET(inet, port)) in
                let out_ch = out_channel_of_descr fd in
                Format.printf "Connected.@.";
                Lwt.return (out_ch, fd)
              with
              | ((Unix.Unix_error(_)) as e) ->
                Unix.close fd;
                out_ch_fd.(i) <- None;
                raise e
            in
            out_ch_fd.(i) <- Some t;
            send_fft v
          | None ->
            Unix.close fd;
            Format.printf "Could not resolve %s@." svr.(i)
    done;
  in
  let ss =
    {
      sample_format = Sample_format_float32le;
      sample_rate = 44100;
      sample_chans = 2;
    }
  in
  let simple =
    Simple.create ~client_name:"Leds" ~dir:Dir_record ~stream_name:"Record" ~sample:ss ~attr ()
  in
  let buflen = 1024 in
  let buf = Bytes.create buflen in

  (* maximum *)
  let max_pw = ref 0. in
  let max_window = 3000. in
  let mult_max = (max_window -. 1.) /. max_window in
  
  let fading = ref 0. in
  let fad_window = 1000. in
  let mult_fad = (fad_window -. 5.) /. fad_window in

  let min_rate = 0.8 in


  while%lwt true do
    Simple.read_bytes simple buf buflen;
    let i = ref 0. in
    Bytes.iter (fun c ->
        let k = (int_of_char c - 128) |> float_of_int in
        i := !i +. k *. k) buf;
    if !max_pw = nan then
      max_pw := 0.;
    let vf = !i |> abs_float |> fun i -> i +. 1. |> log10 in
    let t = Array.init buflen (fun myi ->
        Complex.({ re = float_of_int (int_of_char (Bytes.get buf myi)); im = 0. })) in
    let myfft = Fft.do_fft t in
    let refi = ref 0 in
    let m = ref 0. in
    Format.printf " ";
    let u = 1 in
    Array.iteri (fun k c ->
        if (u-1)*buflen/2/10 < k && k < buflen/2/10*10 then
          begin
            m := (Complex.norm c) +. !m
          end
      ) myfft;
    (*Format.printf "%f@." !m;
    Format.printf "@.";*)

    max_pw := max (mult_max *. !max_pw) vf;
    fading := max (mult_fad *. !fading) vf;
    let v = !fading /. !max_pw -. min_rate |> max 0. |> fun i -> 126. /. (1. -. min_rate) *. i |> int_of_float |> abs |> min 126 in
    send_fft v;
    Lwt_main.yield ()

  done
