open Lwt

let sleepf (sec: float) =
  ignore (Unix.select [] [] [] sec)

let trigger_fork f g n =
  match Unix.fork () with
  | 0 ->
    begin
      try
        f g n; exit 0
      with
      | _ -> exit 0
    end
  | pid -> pid

let (do_fade: (int option -> (int * int * int) -> unit) -> int -> unit) = fun f n ->
  Format.printf "start fade@.";
  for i = 0 to 127/4 do
    f None (4*i, 0, 0);
    sleepf 0.1
  done;
  for i = 0 to 127/4 do
    f None (127 - 4*i, 4*i, 0);
    sleepf 0.1
  done;
  for i = 0 to 127/4 do
    f None (0, 127-4*i, 4*i);
    sleepf 0.1
  done;
  for i = 0 to 127/4 do
    f None (0, 0, 127-4*i);
    sleepf 0.1
  done;
  Format.printf "fade done, exiting@."

let do_center (r, g, b) (r1, g1, b1) f n =
        let c = n/2 in
        for i = 0 to n - 1 do
                f (Some i) (r, g, b);
                sleepf 0.3;
                f (Some i) (r1, g1, b1);
        done;
        for i = 1 to n - 1 do
                let i = n - i - 1 in
                f (Some i) (r, g, b);
                sleepf 0.3;
                f (Some i) (r1, g1, b1);
        done;
        ()

let do_wave (r, g, b) (r1, g1, b1) f n =
        let c = n/2 in
                f (Some (n/2)) (r, g, b);
                sleepf 0.3;
                f (Some (n/2)) (r1, g1, b1);
        for i = 1 to c - 1 do
                f (Some (n/2-1-i-1)) (r/6, g/6, b/6);
                f (Some (n/2-1-i)) (r, g, b);
                f (Some (n/2+i)) (r, g, b);
                f (Some (n/2+i+1)) (r/6, g/6, b/6);
                sleepf 0.3;
                f (Some (n/2-1-i-1)) (r1, g1, b1);
                f (Some (n/2-1-i)) (r1, g1, b1);
                f (Some (n/2+i)) (r1, g1, b1);
                f (Some (n/2+i+1)) (r1, g1, b1);
        done;
        ()

let do_cascade (r, g, b) f n =
        for j = 0 to 25 do
        for i = 0 to n - 1 do
                f (Some i) ((max (r*i*i/n/n) (r*j/25)), ( max (g*i*i/n/n) (g*j/25)), (max (b*i*i/n/n) (b*j/25)))

        done;
        sleepf 0.3;
        done;
        ()

let do_cascade_cent (r, g, b) f n =
        for j = 0 to 25 do
        for i = 0 to n - 1 do
                let i = abs (n/2 - i) in
                f (Some i) ((max (r*i*i/n/n) (r*j/25)), ( max (g*i*i/n/n) (g*j/25)), (max (b*i*i/n/n) (b*j/25)))

        done;
        sleepf 0.3;
        done;
        ()


let do_on f n =
  f None (127, 127, 127)

let do_off f n =
  f None (0, 0, 0)

let do_test f n =
  f None (0, 0, 127);
  sleepf 1.0;
  f None (0, 127, 0);
  sleepf 1.0;
  f None (127, 0, 0);
  sleepf 1.0;
  f None (0, 0, 0)

let do_test_seq f n =
  for i = 0 to n -1 do
    f (Some i) (0, 0, 127);
    sleepf 1.0;
    f (Some i) (0, 127, 0);
    sleepf 1.0;
    f (Some i) (127, 0, 0);
    sleepf 1.0;
    f (Some i) (0, 0, 0);
  done

let do_ulm r r' f n =
        for i = 0 to 2 do
        do_center r r' f n;
        do_center r' r f n;
        done; ()

let follow_port = 3648

exception Break

let anims = [do_fade, "fade";
  do_center Stylesheet.color5 Stylesheet.color6, "center";
  do_center Stylesheet.jaune Stylesheet.violet, "center jv";
  do_center Stylesheet.violet Stylesheet.jaune, "center vj";
  do_wave Stylesheet.color1 Stylesheet.color2, "wave";
  do_cascade Stylesheet.color3, "cascade";
  do_cascade_cent Stylesheet.color4, "cascade center";
  do_wave Stylesheet.color2 Stylesheet.color1, "wave2";
  ]

let follow f n =
  let na = List.length anims in
  let int_to_anim i =
    if i >= na || i < 0 then
      begin
        Format.printf "anim not found, do off@.";
        do_off
      end
    else
      List.nth anims i |> fst
  in
  let open Unix in
  let fd = socket PF_INET SOCK_STREAM 0 in
  let () = bind fd (ADDR_INET(inet_addr_any, follow_port)) in
  let () = Unix.listen fd 1 in
  Format.printf "Follow launched@.";
  while true do
    let fd, client_addr = accept fd in
    begin
      match client_addr with
      | ADDR_INET(addr, port) ->
        Format.printf "Connection from %s:%d@." (string_of_inet_addr addr) (port)
      | _ -> Format.printf "Connection with a Unix socket (unexpected).@."
    end;
    let in_ch = in_channel_of_descr fd in
    try
      while true do
        let c = try
            input_char in_ch
          with
            Unix.Unix_error(_) | End_of_file | Sys_error(_) -> raise Break
        in
        let c = int_of_char c in
        Format.printf "trigger animation %d@." c;
        let a = int_to_anim c in
        ignore (a f n)
      done;
    with
    | Break ->
      Format.printf "Connection closed.@.";
  done; exit 0

exception No_connection

let send_to_follow =
  let out_ch = ref None in
  let open Unix in
  let rec transmit i =
      match !out_ch with
      | None ->
        begin
          let fd = socket PF_INET SOCK_STREAM 0 in
          try
            match Resolv.resolv Config.follower with
            | Some inet ->
              Unix.connect fd (ADDR_INET(inet, follow_port));
              out_ch := Some (out_channel_of_descr fd);
              transmit i
            | None -> raise No_connection
          with
          | Unix.Unix_error(_) | No_connection ->
            Unix.close fd;
            Format.printf "no follower@."
        end
      | Some s ->
        try
          output_char s (char_of_int i);
          flush s;
        with
        | Unix.Unix_error(_) ->
          out_ch := None;
          transmit i
  in
  let rec aux i f a b =
    transmit i;
    f a b;
    Unix.sleep 1
  in
  aux

let looper f n =
  let c = List.length anims in
  while true do
    let a = Random.int c in
    let g, name = List.nth anims a in
    Format.printf "doing %s:%d@." name a;
    send_to_follow a g f n
  done;
  ()

let anims = anims @ [
             do_on, "on";
             do_off, "off";
             do_test, "test";
  do_ulm Stylesheet.violet Stylesheet.jaune, "ulm";
             do_test_seq, "test seq";
]

let anims = List.mapi (fun i (f, n) -> send_to_follow i f, n) anims

let anims = [looper, "looper"] @ anims @ [ (fun _ _ -> Srv.launch_no_fork 3247 |> ignore), "sound";
                      follow, "follow"]
