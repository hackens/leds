let tty_dir = "/dev/"
let tty_prefix = ["ttyAMA"; "ttyACM"; "ttyUSB"; Config.extra_dev]
let baudrate = 115200

open Lwt

exception Device_not_found

let find_dev () =
  let dir = Unix.opendir tty_dir in
  let f = ref None in
  try
  while true do
    let elt = Unix.readdir dir in
    List.iter (fun s ->
        if String.length elt > String.length s && String.sub elt 0 (String.length s) = s then
          f := Some elt;
      ) tty_prefix;
  done;
  assert false
  with
  | End_of_file ->
    Unix.closedir dir;
    match !f with
    | None -> raise Device_not_found
    | Some s -> tty_dir ^ s

let open_com dev =
  let a = Unix.openfile dev [Unix.O_WRONLY;] 0o640 in
  let attr = Unix.tcgetattr a in
  let attr = { attr with Unix.c_obaud = baudrate; Unix.c_icrnl = false; Unix.c_ixon = false; Unix.c_opost = false; Unix.c_ibaud = baudrate; Unix.c_isig = false; Unix.c_icanon = false;
                         Unix.c_echo = false; Unix.c_echoe = false; Unix.c_echok = false; Unix.c_echonl = false; Unix.c_vmin = 0;} in
  Unix.tcsetattr a Unix.TCSANOW attr;
  a

let send_to_leds, close_leds =
  let com = ref None in
  let buf = Bytes.create 4 in
  begin
  fun r g b n ->
    try
      let com = match !com with
        | Some c -> c
        | None ->
          let d = find_dev () in
          Format.eprintf "Opening device %s.@." d;
          let c = open_com d in
          com := (Some c);
          c
      in
      Bytes.set buf 0 (char_of_int (0x80 + n));
      Bytes.set buf 1 (char_of_int (r mod 128));
      Bytes.set buf 2 (char_of_int (g mod 128));
      Bytes.set buf 3 (char_of_int (b mod 128));
      Unix.write com buf 0 4 |> ignore;
    with
    | Device_not_found ->
      Format.printf "Device not connected@.";
    | Unix.Unix_error(e, _, _) ->
      Unix.error_message e |> (^) "unix error while sending commands to the the leds : "  |> Format.printf "%s@.";
      match !com with
      | Some c ->
        Unix.close c;
        com := None
      | _ -> () end
, begin fun () ->
  match !com with
  | None -> ()
  | Some c -> Unix.close c end
